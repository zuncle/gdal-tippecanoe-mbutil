FROM osgeo/gdal:alpine-ultrasmall-latest

MAINTAINER Xavier Thomas <xavier.thomas@pixime.fr>

RUN apk add --no-cache sudo git g++ make curl libgcc libstdc++ sqlite-libs sqlite-dev zlib-dev bash \
 && cd /root \
 && git clone https://github.com/mapbox/tippecanoe.git tippecanoe \
 && cd tippecanoe \
 && cd /root/tippecanoe \
 && make \
 && make install \
 && cd /root \
 && rm -rf /root/tippecanoe

RUN apk add --no-cache python python-dev py-pip
RUN cd /usr/local \
 && git clone git://github.com/mapbox/mbutil.git

RUN apk del git g++ make sqlite-dev

ENV PATH="/usr/local/mbutil:${PATH}"




